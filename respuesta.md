Ejercicio 3. Análisis general

¿Cuántos paquetes componen la captura?
## La captura se corresponde a unos 1050 paquetes.
¿Cuánto tiempo dura la captura?
## La duración de la captura tiene un total de 10.521629838 segundos.
¿Qué IP tiene la máquina donde se ha efectuado la captura?
## La IP de la máquina que ha efectuado la captura es de 192.168.1.116

¿Se trata de una IP pública o de una IP privada?
## Se trata de una IP privada de clase C.
¿Por qué lo sabes?
## Porque por teoría las IPs que corresponden con 10, 172 o 192 son direcciones privadas, pero también existen públicas dentro del rango 192, pero esta no corresponde. 

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
## Los principales protocolos de nivel son UDP, SIP y RTP.
¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
## También podemos ver protocolos de red como ICPM o IPv4.
¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
## Se corresponde con el RTTP del UDP con 134k bytes por egundo, 1031 tramas de los 1050 que se han capturado.
Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo corresponde con una llamada SIP.
Filtra por sip para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

¿En qué segundos tienen lugar los dos primeros envíos SIP?
## Los dos primeros envíos ocurren en el inicio, es decir, en el segundo 0 
Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
## Desde el paquete 6.
Los paquetes RTP, ¿cada cuánto se envían?
## Se envían cada 0.01 aproximadamente.


Ejercicio 4. Primeras tramas
Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

¿De qué protocolo de nivel de aplicación son?
## Se trata del protocolo de nivel UDP.
¿Cuál es la dirección IP de la máquina "Linphone"?
## La dirección IP de la máquina "Linphone" se trata de la 192.168.1.116.
¿Cuál es la dirección IP de la máquina "Servidor"?
## La dirección IP de la máquina "Servidor" es la 212.79.111.155
¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
## Linphone envia un Invite al servidor con una dirección de correo(music@sip.iptel.org).
¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
 ## El servidor contesta con un mensaje de "Status: 100 trying", haciendo saber que el servidor ha recibido el INVITE y se intentará la conexión.

Ahora, veamos las dos tramas siguientes.

¿De qué protocolo de nivel de aplicación son?
## Son de protocolo SIP.
¿Entre qué máquinas se envía cada trama?
## Las máquinas que  hemos hablado anteriormente, una el servidor y otro el cliente (Linphone).
¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
## Que la conexión se ha establecido y se envía un 200 OK al cliente para ello.
¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
## El cliente envía un mensaje ACK, confirmando el recibimiento del mensaje.

Ejercicio 5. Tramas finales
Después de la trama 250, busca la primera trama SIP.

¿Qué número de trama es?
## Se trata del trama número 1042.
¿De qué máquina a qué máquina va?
## Desde el cliente (Linphone, 102.168.1.116) hacia el servidor (212.79.111.155)
¿Para qué sirve?
## El mensaje al tratarse de un BYE, es la traducción para cortar la comunicación entre ambos.
¿Puedes localizar en ella qué versión de Linphone se está usando?
## Se está utilizando la versión User-Agent: Linphone Desktop / 4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37

Ejercicio 6. Invitación
Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
## La dirección SIP con la que se quiere establecer una llamada es music@sip.iptel.org
¿Qué instrucciones SIP entiende el UA?
## Las instrucciones SIP a entender están situadas en la cabecera ALLOW, donde incluye: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUSCRIBE, INFO, PRACK, UPDATE
¿Qué cabecera SIP indica que la información de sesión va en formato SDP?
## La cabecera Content-Type.
¿Cuál es el nombre de la sesión SIP?
## El nombre recibido es Talk.


Ejercicio 7. Indicación de comienzo de conversación
En la propuesta SDP de Linphone puede verse un campo m con un valor que empieza por audio 7078.

¿Qué trama lleva esta propuesta?
## La trama que lleva esta propuesta se trata de la trama 2.
¿Qué indica el 7078?
## Se trata del puerto de transporte.
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## Tiene relación ya que es el puerto donde llegan o se enviarán mensajes.
¿Qué paquetes son esos?
## Los paquetes RTP.

En la respuesta a esta propuesta vemos un campo m con un valor que empieza por audio XXX.

¿Qué trama lleva esta respuesta?
## La trama número 4.
¿Qué valor es el XXX?
## El valor XXXXX es 29448.
¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## Respueta igualitaria al anterior apartado, es un puerto donde llegan o se enviarán mensajes.
¿Qué paquetes son esos?
## Son los paquetes RTP.

Ejercicio 8. Primeros paquetes RTP
Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

¿De qué máquina a qué máquina va?
## Va desde la máquina del cliente (196.168.1.116) hasta el servidor (212.79.111.155)
¿Qué tipo de datos transporta?
## Transporta datos multimedia.
¿Qué tamaño tiene?
## Tiene un tamaño de 214.
¿Cuántos bits van en la "carga de pago" (payload)
## Van un total de 160 bytes, es decir, 1280 bits.
¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
## La periodicidad de los paquetes es cada 0.02 segundos.

Ejercicio 9. Flujos RTP
Vamos a ver más a fondo el intercambio RTP. Busca en el menú Telephony la opción RTP. Empecemos mirando los flujos RTP.

¿Cuántos flujos hay? ¿por qué?
## Se trata de 2 flujos, uno del servidor y cliente al ser una llamada.
¿Cuántos paquetes se pierden?
## Ninguno.
Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
## El valor máximo del delta es de 30.726700 ms.
¿Qué es lo que significa el valor de delta?
## Significa la latencia que se ha generado en la trasmisión y el envío.
¿En qué flujo son mayores los valores de jitter (medio y máximo)?
## Los valores son mayores en los flujos del servidor al cliente.
¿Qué significan esos valores?
## Tienen un significado de el tiempo de retardo en la llegada de los paquetes.
Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción Telephony, RTP, RTP Stream Analysis:

¿Cuánto valen el delta y el jitter para ese paquete?
## Sus valores son:

## Delta: 59.581866 ms ; Jitter: 7.110079 ms
¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
## Sí, mediante el skew.
El "skew" es negativo, ¿qué quiere decir eso?
## Quiere decir que algunos paquetes llegarán más tarde de lo previsto.
En el panel Stream Analysis puedes hacer play sobre los streams:

¿Qué se oye al pulsar play?
## Se escucha la señal de inicio de audio.
¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
## Se oye una disminución en la calidad de la llamada.
¿A qué se debe la diferencia?
## La diferencia es el tamaño del buffer, pues al reducirse, los paquetes que se envían al ser más de lo esperado, no se pueden almacenar y pueden acabar descartándose o perdiéndose.

Ejercicio 10. Llamadas VoIP
Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú Telephony selecciona el menú VoIP calls, y selecciona la llamada hasta que te salga el panel correspondiente:

¿Cuánto dura la llamada?
## La llamada tiene una duración de 10 segundos.

Ahora, pulsa sobre Flow Sequence:

Guarda el diagrama en un fichero (formato PNG) con el nombre diagrama.png.
¿En qué segundo se recibe el último OK que marca el final de la llamada?

Ahora, selecciona los dos streams, y pulsa sobre Play Sterams, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

¿Cuáles son las SSRC que intervienen?
## Intervienen la del cliente (0xD2DB8B4) y la del servidor (0x5C44A34B)
¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
## Se envían un total de 514 paquetes.
¿Cuál es la frecuencia de muestreo del audio?
## La frecuencia de muestreo del audio es de 8kHz.
¿Qué formato se usa para los paquetes de audio (payload)?
## Se usa el formato g711U
.